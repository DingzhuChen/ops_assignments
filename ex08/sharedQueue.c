/******************************************************************************
 * File:         main.c
 * Version:      1.2
 * Date:         2017-10-18
 * Author:       J. Onokiewicz, M. van der Sluys
 * Description:  OPS exercise 5: Queues
 ******************************************************************************/

#define _GNU_SOURCE
#include "Queue.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <signal.h> 

void newHandler ( ) ;

void *producter(void *arg);
void *consumer();
queue_t queue = {NULL};
pthread_mutex_t myMutex = PTHREAD_MUTEX_INITIALIZER;
data_t data = {0, "init"};
volatile bool finish=false,startloop=true;
pthread_t threadID[4];
int main()
{
  struct sigaction  act ;

  // Define SHR :
  memset (&act ,'\0', sizeof ( act ) ) ;
  act.sa_handler= newHandler ;
  act.sa_flags = 0;
  sigemptyset (&act.sa_mask ) ;

  // Install SHR :
  sigaction (SIGINT,&act,NULL) ;
  
  data_t ThreadArgA = {2,"The Thread A"};
  data_t ThreadArgB = {3,"THe THread B"};
  data_t ThreadArgC = {4,"The THread C"};

  
  createQueue(&queue, data);
  pthread_create(&threadID[1],NULL,producter,(void *)&ThreadArgA); 
  pthread_create(&threadID[2], NULL, producter, (void *)&ThreadArgB);
  pthread_create(&threadID[3], NULL, producter, (void *)&ThreadArgC);
  pthread_create(&threadID[4], NULL, consumer,NULL);
  
 
  pthread_join (threadID[1],NULL);
  pthread_join (threadID[2],NULL);
  pthread_join (threadID[3],NULL);
  pthread_join (threadID[4],NULL);
  return 0;
}
void *producter(void *arg)
{  
  data_t temp =*(data_t *)arg;  
  while(startloop)
  {
  sleep(temp.intVal);
  pthread_mutex_lock(&myMutex);
  strcpy(data.text,temp.text);
  pushQueue(&queue, data);
  data.intVal++;

  pthread_mutex_unlock(&myMutex);

  }
}

void *consumer(){
   while(startloop)
   { 
      data_t datainit={0,"init"};
      sleep(6);
      pthread_mutex_lock(&myMutex);
      showQueue(&queue);
      PrintIntoFlie(&queue,"txtfile.txt");
      deleteQueue(&queue);
      finish = true;
     
      createQueue(&queue,datainit);
      finish = false;
      data.intVal=0;
      pthread_mutex_unlock(&myMutex);
      
   }

}

void newHandler () {


  while(finish!=true){;}
  startloop=false;
  
  exit(EXIT_SUCCESS);
}
