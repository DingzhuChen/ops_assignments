/******************************************************************************
 * File:         display.c
 * Version:      1.4
 * Date:         2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include "displayFunctions.h"
#include <sys/types.h>
#include <sys/resource.h>

int main(int argc, char *argv[]) {
  unsigned long int numOfTimes,niceIncr;
  char printMethod;
  char printChar;
  ErrCode err;
  int  ichild;
  
  err = SyntaxCheck(argc, argv);  // Check the command-line parameters
  if(err!= NO_ERR) {
    DisplayError(err);        // Print an error message
  } else {
    
    printMethod = argv[1][0];
    
    numOfTimes = strtoul(argv[2], NULL, 10);  // String to unsigned long
    
    niceIncr= strtoul(argv[3],NULL,10);

    for(ichild=0; ichild<argc-4 ;ichild++)
    {
        int i=fork();
          switch(i)
          {
            case -1://failed
              printf("failed");
            break;
            case 0://child
              nice(niceIncr*ichild);
              printf("\nnice value:%d,is the %d chirld\n",getpriority(PRIO_PROCESS,getpid()),ichild);
              PrintCharacters(printMethod, numOfTimes, argv[4+ichild][0]);
              exit(0);// this has to be there otherwise after the switch the fork will goes again.
            break;
            default:
            break;
          }      
    }
    for(ichild=0; ichild<argc-4 ;ichild++){
      wait(NULL);
    }
    
    
//PrintCharacters(printMethod, numOfTimes, printChar);  // Print character printChar numOfTimes times using method printMethod
  }
  //printf("%d",argc-1);
  printf("\nparent done\n");  // Newline at end
  return 0;
}
