#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
{
  int pipeFD[2];  //pip 1
  int pipeFD1[2]; //pip 1
  int i = 1;
  char buf;
  char message[22];
  // Create pipe :
  // Create pipe :
  if (pipe(pipeFD) == -1)
  {
    perror(" pipe ");
    exit(1);
  }
  if (pipe(pipeFD1) == -1)
  {
    perror(" pipe ");
    exit(1);
  }

  // Fork off child process :
  switch (fork())
  {
  case -1:
    perror(" fork ");
    exit(1);
  case 0:

    // Child - writes output of ls to pipe :
    close(pipeFD[0]);   // Close output end - not necessary , but unused
    dup2(pipeFD[1], 1); //disable the 1 replace with pipfie descriptror1 inut
    close(pipeFD[1]);   //

    close(pipeFD1[1]); // Close input end - not necessary , but unused
    dup2(pipeFD1[0], 0);
    close(pipeFD1[0]); //
    execlp("./filter", "filter", NULL);
    perror(" execlp ");
    exit(1); // Starts ls and should never return

  default:

    // Parent - reads from pipe and writes to stdOut :

    // We MUST close the unused write end , since the read () call suspends the
    // parent when the pipe is empty ( and a write end still exists ) . The other
    // write end disappears as soon as ls exits , since the process terminates .
    close(pipeFD[1]);  // Try commenting this out
    close(pipeFD1[0]); // close output
    read(0, &buf, 1);
     write(pipeFD1[1], &buf, 1);
    while (buf != 0x1B)
    {

      
      
    
      i=read(pipeFD[0], &buf, 1);
      if(i>0){
	write(1, &buf, 1);
	write(1, &buf, 1);
      }
  
      read(0, &buf, 1);
      write(pipeFD1[1], &buf, 1);
    }

    wait(NULL);
    close(pipeFD[0]);
    close(pipeFD1[1]); // tidy up

    // Wait for child
    // Tidy up
    exit(0);
  }
}
