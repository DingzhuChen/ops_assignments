#include <sys/wait.h> // wait()
#include <stdio.h> // perror()
#include <stdlib.h> // exit()
#include <unistd.h> // pipe(),dup*(), fork(),exec*(), read(),write(),close()
#include </home/student/Projects/OPS_assignments/OPS-master/ex07/filter.c>
#define ESC 0x1B
int main(void) 
{

  extern char letter;
  //read(0, &letter, 1); 

  int pipeFD[2];
  //char buf;
  // Create pipe:
  if(pipe(pipeFD) == -1) //int pipe(int fd[2]);create an unnamed pipe
  {
     perror("pipe");
     exit(EXIT_FAILURE);
   }
   // Fork off child process:
   switch(fork()) 
   {
      case -1:
         perror("fork");
         exit(EXIT_FAILURE);
      case 0: // Child - writes output of ls to pipe:
         close(pipeFD[0]); // Close read end - not necessary , but unused
         dup2(pipeFD[1], 1); // Close(1) and dup(pipeFD[1]) to FD 1 in 1 call
         close(pipeFD[1]); // Only stdOut (FD 1) open, redir to pipeFD[0]
	execl("./filter", "filter", (char *) NULL);
	//char letter;
  	/*read(0, &letter, 1); 
         while(letter != ESC) {        // ESC = Ctrl-[ in shell
	    	 letter = toupper(letter);
		 write(1, &letter, 1);
	 	 read(0, &letter, 1);
	 }*/
         perror("execlp");
         exit(EXIT_FAILURE);

      default: // Parent - reads from pipe and writes to stdOut:
         close(pipeFD[1]); // Still open input part of the pipe in parent, it will be still waiting for information if it still has an open input part of the pipe
         //If I keep open the input side of the pipe, then the read thinks, oh there is a pipe, I’m still expecting some input, so it will keep waiting for more 

         // Read from pipe and write to stdout:
         while(read(pipeFD[0], &letter, 1) > 0) write(1, &letter, 1);
         wait(NULL); // Wait for child
         close(pipeFD[0]); // Tidy up
         exit(EXIT_SUCCESS);
   }
}
