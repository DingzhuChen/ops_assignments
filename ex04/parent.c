/******************************************************************************
 * File:         parent.c
 * Version:      1.4
 * Date:         2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include "displayFunctions.h"
//#include <sys/types.h>
#include <sys/resource.h>

int main(int argc, char *argv[]) {
  unsigned long int numOfTimes,niceIncr;
  ErrCode err;
  int  ichild;
  
  err = SyntaxCheck(argc, argv);  // Check the command-line parameters
  if(err!= NO_ERR) {
    DisplayError(err);        // Print an error message
  } else {
    

    
    niceIncr= strtoul(argv[3],NULL,10);
  
    for(ichild=0; ichild<argc-4 ;ichild++){
    int i=fork();
     switch(i){
      case -1://failed
	printf("failed");
      break;
      
      case 0://child
	 nice(niceIncr*ichild);
 	 printf("\n nice value:%d,is the %d chirld\n",getpriority(PRIO_PROCESS,getpid()),ichild);
	 execl("../ex02/display","display",argv[1], argv[2] , argv[ichild+4],(char *)NULL);
	 perror( " Starting child failed .\ n " ) ;
   //当execl成功执行时，execl下一行的printf就被忽略了；只有execl执行失败了，下一行printf才能执行
	 
	 exit(0);// this has to be there otherwise after the switch the fork will goes again.
         break;
      
      default:
       
	break;
      }      
    }
    for(ichild=0; ichild<argc-4 ;ichild++){
      wait(NULL);
    }
     printf("All children have finished.\n"); 
    
//PrintCharacters(printMethod, numOfTimes, printChar);  // Print character printChar numOfTimes times using method printMethod
  }
  //printf("%d",argc-1);
  printf("\n");  // Newline at end
  return 0;
}
