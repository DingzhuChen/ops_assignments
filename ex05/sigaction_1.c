# define _POSIX_C_SOURCE 199309L 
# include <stdio.h>

# include <string.h> //
# include <signal.h> //


volatile sig_atomic_t signalCount = 0;

void newHandler ( int sig ) ;


int main ( void ) {
  struct sigaction  act, oldact ;

  // Define SHR :
  memset (&act ,'\0', sizeof ( act ) ) ;
  act.sa_handler= newHandler ;
  act.sa_flags = 0;
  sigemptyset (&act.sa_mask ) ;

  // Install SHR :
  sigaction (SIGINT,&act,&oldact) ;

  printf ( " Counting Ctrl - Cs until you press Enter ...\ n " ) ;
  while (getchar() != '\n') {
    ;
  }
  printf ("\nCtrl - C has been pressed % d times . Press Ctrl - C again to exit .\n", signalCount) ;

  sigaction ( SIGINT , &oldact , NULL ) ;

  while (1) ;

  return 0;
}
// SHR using sa_handler :
void newHandler ( int sig ) {
  printf ( " -- Signal caught : %d\n" , sig ) ;
  signalCount ++;
}
