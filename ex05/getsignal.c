# define _POSIX_C_SOURCE 199309L 
# include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
# include <string.h> //
# include <signal.h> //


volatile sig_atomic_t signalCount = 0;

void newHandler ( int sig ) ;


int main ( void ) {
char ds[2];

struct sigaction  act, oldact ;
  // Define SHR :
  memset (&act ,'\0', sizeof ( act ) ) ;
  act.sa_handler= newHandler ;
  act.sa_flags = 0;
  sigemptyset (&act.sa_mask ) ;
  // Install SHR :
  sigaction (25,&act,&oldact) ;

  printf("%d\n",getpid());
  while (1) {
     if(signalCount==10){signalCount=0;}
     sprintf(ds,"%d",signalCount);
     write(1,&ds,sizeof(char));
   
    sleep(1);
   // signalCount ++;
  }
  //sigaction ( SIGI25NT , &oldact , NULL ) ;
  return 0;
}
// SHR using sa_handler :
void newHandler ( int sig ) {
  signalCount ++;
}
